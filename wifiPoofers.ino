









/*
  Flash Mode "QIO"
  Flash Frequency "40MHz"
  Upload Using "Serial"
  CPU Frequency "80 MHz"
  Flash Size "4M"
  Reset Method "nodemcu"
*/

////// poofer_miko_explode_800
/////// poofer_boom_explode_800
//// tcp service on port 23
//// name is ESP_chipID

#include <Wire.h>
#include <Adafruit_LEDBackpack.h>
#include <Adafruit_MPR121.h> //touch sensor
#include <Adafruit_GFX.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
ESP8266WiFiMulti WiFiMulti;
//for telnet
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>         //https://github.com/tzapu/WiFiManager
#include <ESP8266mDNS.h>


unsigned long myTime = 0; //= micros();
bool reachOut = false;
int startTime;


const char *ssid     = "router";
const char *password = "password";

const int numberOfFrames = 9;
const int numberOfPoofers = 4;
int explosionTime[numberOfPoofers]; //a value for how big of explosion to make
int frameBuffer[numberOfFrames][numberOfPoofers];
int frameNumber = 0;
bool framesFull = false;
int playMultiply = 90; //how many milliseconds to multiply the poof size by
int frameLength = 1000; //milliseconds, how long should a frame take to play

int lastPooferNumber = -1;

int expectedNumberExploders = 3;
#define MAX_SRV_CLIENTS 5
String telnetData[MAX_SRV_CLIENTS];

char hostString[16] = {0};//mDNS

String APprefix = "explosion_controller "; //what to prefix the access point name with for recognition by other devices
String APssid = APprefix + String(ESP.getChipId());


//WiFiServer telnetServer(23); //telnet port 23
WiFiClient serverClients[MAX_SRV_CLIENTS];


Adafruit_BicolorMatrix matrix = Adafruit_BicolorMatrix();
Adafruit_MPR121 cap = Adafruit_MPR121();

uint16_t lasttouched = 0;
uint16_t currtouched = 0;



String exploderHostname[MAX_SRV_CLIENTS];
IPAddress exploderIP[MAX_SRV_CLIENTS]; //each number of the IP gets it's own slot in the array
int numberServicesAvalible; //number of explosion machines found on the network

unsigned long machineTime;
unsigned long lastMachineTime;
int touchDelay = 175; //milliseconds of machineTime
int defaultWorldPoof = 200; //when you dont know what to do
bool hookedUpEverything = false;
bool didSomething = false;
long idleCount = 0;
int lookingLong = 0;
bool pooferTouched = false;

const bool hold = true;
const bool explode = false;
bool hORp = explode; 
bool autoPoof = true;

IPAddress miko = {192, 168, 1, 48};
const char * host = "192.168.1.1";

String IPtoString(IPAddress ipa);
void searchNetworkServices();
void updateHPdisplay();

int hpPixelX = 2;
int hpPixelY = 2;

///////////////text on LED matrix
void matrixText(String hello, String world) {
  matrix.setTextWrap(false);  // we dont want text to wrap so it scrolls nicely
  matrix.setTextSize(1);
  matrix.setTextColor(LED_GREEN);
  for (int8_t x = 7; x >= -36; x--) {
    matrix.clear();
    matrix.setCursor(x, 0);
    matrix.print(hello);
    matrix.writeDisplay();
    delay(30);
  }
  matrix.setRotation(3);
  matrix.setTextColor(LED_RED);
  for (int8_t x = 7; x >= -36; x--) {
    matrix.clear();
    matrix.setCursor(x, 0);
    matrix.print(world);
    matrix.writeDisplay();
    delay(30);
  }


}//end text

void matrixTextOne(String hello) {
  int textScrollRows = (hello.length() * 5);
  matrix.setTextWrap(false);  // we dont want text to wrap so it scrolls nicely
  matrix.setTextSize(1);
  matrix.setTextColor(LED_GREEN);
  for (int8_t x = 7; x >= -textScrollRows; x--) {
    matrix.clear();
    matrix.setCursor(x, 0);
    matrix.print(hello);
    matrix.writeDisplay();
    delay(30);
  }
 
  //matrix.setRotation(0);

}//end text

static const uint8_t PROGMEM
smile_bmp[] =
{ B00111100,
  B01000010,
  B10100101,
  B10000001,
  B10100101,
  B10011001,
  B01000010,
  B00111100
},
neutral_bmp[] =
{ B00111100,
  B01000010,
  B10100101,
  B10000001,
  B10111101,
  B10000001,
  B01000010,
  B00111100
},
empty_bmp[] =
{ B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000
},
pirateFace_bmp[] =
{ B00001000,
  B01100110,
  B01100110,
  B00000001,
  B00000000,
  B01111110,
  B01111110,
  B00000000
},
thumb_bmp[] =
{ B00000000,
  B00011000,
  B00011000,
  B00111110,
  B11111110,
  B11111110,
  B00111110,
  B00000000
};




void displayHappy() {
  matrix.clear();
  matrix.drawBitmap(0, 0, smile_bmp, 8, 8, LED_GREEN);
  matrix.writeDisplay();
}

void displayPirate() { // left/right, up/down for pixel locations, 0,0 top left
  matrix.clear();
  matrix.drawBitmap(0, 0, pirateFace_bmp, 8, 8, LED_RED);
  matrix.writeDisplay();
}

void displayThumb() {
  matrix.clear();
  matrix.drawBitmap(0, 0, thumb_bmp, 8, 8, LED_GREEN);
  matrix.writeDisplay();
}

void displayPlay(){
  matrix.clear();
  matrix.drawLine(2, 1, 5, 4, LED_GREEN);
  matrix.drawLine(5, 4, 2, 7, LED_GREEN);
  matrix.drawLine(2, 7, 2, 1, LED_GREEN);
  matrix.writeDisplay();
  
}

void displayBored() {
  matrix.clear();
  matrix.drawBitmap(0, 0, neutral_bmp, 8, 8, LED_YELLOW);
  matrix.writeDisplay();
}

void testDisplay() {

  matrix.clear();
  matrix.drawBitmap(0, 0, smile_bmp, 8, 8, LED_GREEN);
  matrix.writeDisplay();
  delay(500);

  matrix.clear();
  matrix.drawRect(0, 0, 8, 8, LED_RED);
  matrix.fillRect(2, 2, 4, 4, LED_GREEN);
  matrix.writeDisplay();  // write the changes we just made to the display
  delay(500);

  matrix.clear();
  matrix.drawBitmap(0, 0, neutral_bmp, 8, 8, LED_YELLOW);
  matrix.writeDisplay();
  delay(500);

  matrix.clear();
  matrix.drawCircle(3, 3, 2, LED_YELLOW);
  matrix.writeDisplay();  // write the changes we just made to the display
  delay(500);
} //end testDisplay()
/////////////////////////////////////////////////////////////////////////////   end display


///////////////////////////////////////////////////////////// messages
void sendAirMessage(int j, String bMessage){ // j is which client to send it to (4+j), bMessage is what to tell it
 // WiFiClient serverClients[MAX_SRV_CLIENTS];
  if (!serverClients[j].connected()){
    serverClients[j].stop();
    serverClients[j].connect(MDNS.IP(j), 23);
  }

  serverClients[j].print(bMessage.c_str());
  //serverClients[j].stop();
  
    
}


void poof(String targetPoofer, int poofLength) {
  String broadcastMessage;

  broadcastMessage = "poofer_" + targetPoofer + "_explode_" + String(poofLength);

  
  for (int i = 0; i < numberServicesAvalible; i++) {
    
    if (targetPoofer == MDNS.hostname(i)) sendAirMessage(i, broadcastMessage);
        
  }



}//end poof

void poofAll(){
  displayPirate();
  String broadcastMessage;

  for (int i = 0; i < numberServicesAvalible; i++) {
    broadcastMessage = "explodeAll"; 
    sendAirMessage(i, broadcastMessage);
         
  }
  
delay(touchDelay);
matrix.clear();
updateHPdisplay();
lastPooferNumber = -1;
}



void broadcastMyIP() {
  
  sprintf(hostString, "main");         //comes out lower case
  WiFi.hostname(hostString);

  MDNS.begin(hostString);
  
  MDNS.addService("con", "tcp", 23); // Announce con tcp service on port 23

yield();
}// end broadcastMyIP()


void findServices(){
  numberServicesAvalible = MDNS.queryService("exp", "tcp"); // Send out query for exp tcp services
  reachOut = false;
}



void searchNetworkServices() {

  //  Sending mDNS query
  numberServicesAvalible = MDNS.queryService("exp", "tcp"); // Send out query for exp tcp services
  // mDNS query done
  // if (numberServicesAvalible == 0) {
  //    no services found
  // } else {
 // //   numberServicesAvalible services found
  //  for (int i = 0; i < numberServicesAvalible; i++) {
  // details for each service found
   //  exploderHostname[i] = MDNS.hostname(i);
    //exploderIP[i] = MDNS.IP(i); //
  //  //  //MDNS.port(i); //assuming 23
   // }//end for
  // }//end else
}// end searchNetworkServices()

////////////how do I connect to these services after finding them??

////////////////////////////////////////////////////////////////////////////   setup
void setup() {
  WiFiManager wifiManager;

  //go into access point mode and serve a webpage askng for an ssid and PW
  wifiManager.autoConnect(APssid.c_str());
  
  //am I online, try 20 times, then go back to access point mode if not
  uint8_t i = 0;
  while (WiFi.status() != WL_CONNECTED && i++ < 20) delay(500);
  if (i == 21) {
    wifiManager.autoConnect(APssid.c_str()); //this is being used to log on to wifi router, or ask for logon details if it cant
  }
  // yay I'm connected


  //searchNetworkServices();
 // telnetServer.begin();
 // telnetServer.setNoDelay(true);

  broadcastMyIP();

findServices();
  matrix.begin(0x70);  // pass in the address for LED display
  cap.begin(0x5A); //address for touch sensors
  cap.setThreshholds(1, 1); //0-255

  matrix.setBrightness(15);
  matrix.setRotation(3);
  //matrixText("start", "up");
  matrixTextOne("Syncytium");
  delay(400);
  matrix.clear();
  updateHPdisplay();

}//end setup()



///////////////////////////////////touch section

void updateHPdisplay(){
  if (hORp == explode){
    //clear reel lines
    //matrix.drawLine(0, hpPixelY, 2, hpPixelY, LED_OFF); //dark left movie reel lines
    //matrix.drawLine(0, (hpPixelX +3), 2, (hpPixelX +3), LED_OFF);
    //matrix.drawLine(5, hpPixelY, 7, hpPixelY, LED_OFF); //dark right movie reel lines
    //matrix.drawLine(5, (hpPixelX +3), 7, (hpPixelX +3), LED_OFF);
    matrix.clear();
    if (autoPoof == true){// get that dot for last poofed back on screen
      if (lastPooferNumber == 0) matrix.drawPixel(0, 0, LED_RED);
      if (lastPooferNumber == 1) matrix.drawPixel(7, 0, LED_RED);
      if (lastPooferNumber == 2) matrix.drawPixel(0, 7, LED_RED);
      if (lastPooferNumber == 3) matrix.drawPixel(7, 7, LED_RED);
    }
    matrix.drawRect(hpPixelX, hpPixelY, (hpPixelX +2), (hpPixelY +2), LED_GREEN);
    matrix.writeDisplay();
  }
  if (hORp == hold){
   matrix.drawRect(hpPixelX, hpPixelY, (hpPixelX +2), (hpPixelY +2), LED_YELLOW);
   matrix.drawLine(0, hpPixelY, 2, hpPixelY, LED_YELLOW); //draw left movie reel lines
   matrix.drawLine(0, (hpPixelX +3), 2, (hpPixelX +3), LED_YELLOW);
   
   matrix.drawLine(5, hpPixelY, 7, hpPixelY, LED_YELLOW); //draw right movie reel lines
   matrix.drawLine(5, (hpPixelX +3), 7, (hpPixelX +3), LED_YELLOW);
   matrix.writeDisplay();
  }
}

void updatePooferLines(){

  if (explosionTime[0] > 0 || explosionTime[1] > 0 || explosionTime[2] > 0 || explosionTime[3] > 0){
    matrix.drawLine(1, 0, (explosionTime[0] +1), (explosionTime[0]), LED_RED); // left/right, up/down for pixel locations, 0,0 top left
    matrix.drawLine(6, 0, (6 - explosionTime[1]), explosionTime[1], LED_RED);
    matrix.drawLine(1, 7, (explosionTime[2] +1), (7 - explosionTime[2]), LED_RED);
    matrix.drawLine(6, 7, (6 - explosionTime[3]), (7 - explosionTime[3]), LED_RED);
    matrix.writeDisplay();
  }//end if
}// updatePooferLines()

void pooferTouch(int pooferNumber) { //pooferNumber 0-3
  //explosionTime[4];

  
  lastPooferNumber = pooferNumber;
  if (autoPoof == false){
  matrix.clear();
  explosionTime[pooferNumber]++;
  updateHPdisplay();
  updatePooferLines();
  
  //matrix.drawLine(pooferNumber, 0, pooferNumber, explosionTime[pooferNumber], LED_RED);
  if (explosionTime[pooferNumber] > 7){
    explosionTime[pooferNumber] = 0;
    matrix.clear();   
    updateHPdisplay();   
  }
  


  }// end    if (autoPoof == false)

  if (autoPoof == true){
     matrix.clear();
    if (pooferNumber == 0) matrix.drawPixel(0, 0, LED_RED);
    if (pooferNumber == 1) matrix.drawPixel(7, 0, LED_RED);
    if (pooferNumber == 2) matrix.drawPixel(0, 7, LED_RED);
    if (pooferNumber == 3) matrix.drawPixel(7, 7, LED_RED);
    updateHPdisplay();
    poof(MDNS.hostname(pooferNumber), 200); //poof default 500 on the current single poofer
}


}//end PooferTouch

void holdOrPoof(){
  
  hORp = !hORp;

  if (hORp == explode){ //if it was hold, then explode
    updateHPdisplay();
    for (int u; u < numberOfPoofers; u++){
      if (explosionTime[u] > 0) poof(MDNS.hostname(u), (explosionTime[u] * playMultiply));
      explosionTime[u] = 0;
    }
    lastPooferNumber = -1;
    updateHPdisplay();
    autoPoof = true;

  
  }else if (hORp == hold){ //if it was explode, then hold
    matrix.clear();  
    updateHPdisplay();
    autoPoof = false;
   
  }
  
  //extra touch delay, in addition to the one after checkTouches()
   //delay(touchDelay);
}//end holdOrPoof

void nextFrame(){
  //frameNumber = 0;
 
  //get all the last frame's poofer explode times into an array
  for (int i = 0; i < numberOfPoofers; i++) {
    frameBuffer[frameNumber][i] = explosionTime[i]; //frameBuffer[numberOfFrames][numberOfPoofers] = explosionTime[pooferNumber];
    explosionTime[i] = 0;
    delay(10);
  }
  
  matrix.clear();
  updateHPdisplay();
  frameNumber++;
  if (frameNumber >= numberOfFrames){
    framesFull = true;
    }
  //checkTouches();
 
}//end nextFrame()

void framesFilled(){
      //matrix.drawLine(pooferNumber, 0, pooferNumber, explosionTime[pooferNumber], LED_RED);  up, right
    matrix.drawRect(hpPixelX, hpPixelY, (hpPixelX +2), (hpPixelY +2), LED_OFF);
    matrix.writeDisplay();
    delay(touchDelay);
    matrix.drawRect(hpPixelX, hpPixelY, (hpPixelX +2), (hpPixelY +2), LED_RED);
    matrix.writeDisplay();
    delay(touchDelay);
    matrix.drawRect(hpPixelX, hpPixelY, (hpPixelX +2), (hpPixelY +2), LED_OFF);
    matrix.writeDisplay();
    delay(touchDelay);
    matrix.drawRect(hpPixelX, hpPixelY, (hpPixelX +2), (hpPixelY +2), LED_RED);
    matrix.writeDisplay();
    matrix.clear();
  updateHPdisplay();
}//end frames filled

void playFrames(){
  String broadcastMessage;
    matrix.clear();
    displayPlay();

for (int framePlay = 0; framePlay <= frameNumber; framePlay++){
  for (int i = 0; i < numberServicesAvalible; i++) { 
    poof(MDNS.hostname(i), (frameBuffer[framePlay][i] * playMultiply));  //frameBuffer[numberOfFrames][numberOfPoofers];
    
  }// end for
  delay(frameLength); 
  
}// end for
  //framesFull = false;
  delay(touchDelay);
  matrix.clear();
  updateHPdisplay();

  


}//end play frames


void clearFrames(){


  //double for to set all values to zero
  for (int i = 0; i < numberOfPoofers; i++){
    explosionTime[i] = 0;
    for (int framePlay = 0; framePlay <= numberOfFrames; framePlay++){
      frameBuffer[framePlay][i] = 0;
      }
  }//end double for
  framesFull = false;

  matrix.clear();
  updateHPdisplay();
  frameNumber = 0;
 lastPooferNumber = -1;
}//end clearFrames

void previousFrame(){
   //get all the last frame's poofer explode times into an array
  for (int i = 0; i < numberServicesAvalible; i++) {
    frameBuffer[frameNumber][i] = explosionTime[i]; //frameBuffer[numberOfFrames][numberOfPoofers] = explosionTime[pooferNumber];
    explosionTime[i] = 0;
  }
  
  matrix.clear();
  if (frameNumber <8 ) matrix.drawLine(0, 7, 0, frameNumber,  LED_YELLOW);
  if (frameNumber >7 && frameNumber <14 ){
    matrix.drawLine(0, 7, 0, frameNumber,  LED_YELLOW);
    matrix.drawLine(7, 7, 7, (7 - frameNumber),  LED_YELLOW);
  }
  if (frameNumber >14 ){
    matrix.drawLine(0, 7, 0, frameNumber,  LED_YELLOW);
    matrix.drawLine(7, 7, 7, (7 - frameNumber),  LED_YELLOW);
    matrix.drawLine(7, 0, frameNumber, 0,  LED_YELLOW);
  }
  
  matrix.writeDisplay();
  frameNumber++;
  if (frameNumber >= numberOfFrames){
    framesFull = true;
    }
  //checkTouches();
 
 

}//end previousFrame

void mic(){
  
}

void checkTouches() {

  //poof("miko", 500)
  //matrix.fillRect(3,0, 4,1, LED_GREEN);
  //if (currtouched != 0) displayThumb();
  currtouched = cap.touched(); // these are the touch sensor pins, and what they do!
  //didSomething
  if (bitRead(currtouched, 0)) pooferTouch(0); //which poofer 0,1,2,3
  if (bitRead(currtouched, 1)) pooferTouch(1); //which poofer 0,1,2,3
  if (bitRead(currtouched, 2)) pooferTouch(2); //which poofer 0,1,2,3
  if (bitRead(currtouched, 3)) pooferTouch(3); //which poofer 0,1,2,3
 
  if (bitRead(currtouched, 4)) poofAll();// 
//  if (bitRead(currtouched, 5)) nextFrame();//

  //next frame
  if (autoPoof == false && framesFull == false && bitRead(currtouched, 5)) nextFrame();
  if (autoPoof == false && framesFull == true && bitRead(currtouched, 5)) framesFilled(); 

  if (bitRead(currtouched, 6)) playFrames();
  if (bitRead(currtouched, 7)) clearFrames();
  if (bitRead(currtouched, 8)) previousFrame();
  if (bitRead(currtouched, 9)) ;//beam poofAll();
  if (bitRead(currtouched, 10)) mic();
  if (bitRead(currtouched, 11)) holdOrPoof();
  if (currtouched == 0 && numberServicesAvalible < expectedNumberExploders) findServices();

 if (currtouched != 0) delay(touchDelay);
 //yield();
  // reset our state
  lasttouched = currtouched;
} // end checkTouches()

//buttons ///hardware is lijke this
// 0-3 for each poofer, hold for poof strength, or touch for default
// 4 - hold/poof. wait until this is off to poof. switches between instant and programming modes
// 5 - all the poofers in 1 blast
// 6 - next frame, acts like poof except just advances one frame in sequence writing
// 7 - play frames
// 8 - clear frames
// 9 - previous frame
// 10 - play frames
// 11 - play frames to audio
// 12 - turn beam break on/off
//
//


String IPtoString(IPAddress ipa){
    
    String str = String(ipa[0]);
    str += ".";
    str += String(ipa[1]);
    str += ".";
    str += String(ipa[2]);
    str += ".";
    str += String(ipa[3]);
    
    return str;
}// end IP to string



void loop() {


  //check for poofers every 60 seconds
 // int checkForServicesInterval = 30000; //60000 = 60seconds * 1000ms in a second
 // if (myTime == 0) startTime = millis();
 // myTime = millis();
 // if ((myTime - startTime) > checkForServicesInterval){
 //   reachOut = true;
//    startTime = 0;
 // }

  // || reachOut == true) findServices(); //this has the potential to reduce expectedNumberExploders
  //webServer.handleClient();
  
  checkTouches();




}
